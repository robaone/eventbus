package com.robaone.gwt.eventbustest.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.eventbus.client.ComposeEvent;
import com.robaone.gwt.eventbus.client.EventBus;
import com.robaone.gwt.eventbus.client.ObjectChannelEvent;
import com.robaone.gwt.eventbus.client.controller.PagedListController;
import com.robaone.gwt.eventbus.client.model.PagedData;
import com.robaone.gwt.eventbus.client.ui.PaginationButtonsUi;
import com.robaone.gwt.eventbus.client.widget.DynamicTableRowWidget;
import com.robaone.gwt.eventbus.client.widget.DynamicTableWidget;
import com.robaone.gwt.eventbus.client.widget.ListItemWidget;
import com.robaone.gwt.eventbus.client.widget.OrderedListWidget;
import com.robaone.gwt.eventbus.client.widget.UnorderedListWidget;
import com.robaone.gwt.eventbustest.shared.CustomMessage;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EventBusTest implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	public static final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		setupTest5();
		setupTest6();
		runJsTests();
		runEventTests();
		runUnOrderedListTests();
		runOrderedListTests();
		runDynamicTableTests();
		runPagedButtonTests();
	}

	private void runPagedButtonTests() {
		PaginationButtonsUi buttons = new PaginationButtonsUi();
		String id = "pagedtest1";
		setTestContent(id,buttons);
		PagedData data = new PagedData();
		
		PagedListController controller = new PagedListController(buttons,data);
	}

	private void runDynamicTableTests() {
		String id = "tabletest1";
		DynamicTableWidget table = createTableTest1();
		setTestContent(id, table);
		id = "tabletest2";
		DynamicTableWidget table2 = createTableTest2();
		setTestContent(id, table2);
		id = "tabletest3";
		DynamicTableWidget table3 = createTableTest3();
		setTestContent(id, table3);
	}

	private DynamicTableWidget createTableTest2() {
		DynamicTableWidget table = new DynamicTableWidget();
		Widget[] headers = new Widget[2];
		headers[0] = new Label("Column1");
		headers[1] = new Label("Column2");
		table.setHeaders(headers);
		Widget[] row1 = new Widget[2];
		row1[0] = new Label("Row1Column1");
		row1[1] = new Button("Row1Column2");
		DynamicTableRowWidget row1w = new DynamicTableRowWidget();
		row1w.setContent(row1);
		Widget[] row2 = new Widget[2];
		row2[0] = new Label("Row2Column1");
		row2[1] = new Button("Row2Column2");
		DynamicTableRowWidget row2w = new DynamicTableRowWidget();
		row2w.setContent(row2);
		DynamicTableRowWidget[] rows = new DynamicTableRowWidget[2];
		rows[0] = row1w;
		rows[1] = row2w;
		table.setRows(rows);
		return table;
	}
	
	private DynamicTableWidget createTableTest3() {
		DynamicTableWidget table = new DynamicTableWidget();
		Widget[] headers = new Widget[2];
		headers[0] = new Label("Column1");
		headers[1] = new Label("Column2");
		table.setHeaders(headers);
		Widget[] row1 = new Widget[2];
		row1[0] = new Label("Row1Column1");
		row1[1] = new Button("Row1Column2");
		DynamicTableRowWidget row1w = new DynamicTableRowWidget();
		row1w.setContent(row1);
		Widget[] row2 = new Widget[2];
		row2[0] = new Label("Row2Column1");
		row2[1] = new Button("Row2Column2");
		DynamicTableRowWidget row2w = new DynamicTableRowWidget();
		row2w.setContent(row2);
		DynamicTableRowWidget[] rows = new DynamicTableRowWidget[2];
		rows[0] = row1w;
		rows[1] = row2w;
		table.setRows(rows);
		rows = new DynamicTableRowWidget[1];
		rows[0] = row1w;
		table.setRows(rows);
		return table;
	}

	private DynamicTableWidget createTableTest1() {
		DynamicTableWidget table = new DynamicTableWidget();
		String[] headers = {"Column1","Column2"};
		table.setHeaders(headers);
		DynamicTableRowWidget row = new DynamicTableRowWidget();
		String[] row1 = {"Row1Column1","Row1Column2"};
		row.setContent(row1);
		table.appendRow(row);
		row = new DynamicTableRowWidget();
		String[] row2 = {"Row2Column1","Row2Column2"};
		row.setContent(row2);
		table.appendRow(row);
		return table;
	}

	private void setTestContent(String id, Widget table) {
		RootPanel.get(id).getElement().getFirstChild().removeFromParent();
		RootPanel.get(id).add(table);
		RootPanel.get(id).getElement().setAttribute("class", "pass");
	}

	private void runUnOrderedListTests() {
		UnorderedListWidget ol = new UnorderedListWidget();
		ListItemWidget li = new ListItemWidget();
		ol.add(li);
		li.add(new Label("UnorderdListWidget creation successful"));
		RootPanel.get("oltest1").getElement().getFirstChild().removeFromParent();
		RootPanel.get("oltest1").add(ol);
		RootPanel.get("oltest1").getElement().setAttribute("class", "pass");
	}
	
	private void runOrderedListTests() {
		OrderedListWidget ol = new OrderedListWidget();
		ListItemWidget li = new ListItemWidget();
		ol.add(li);
		li.add(new Label("OrderdListWidget creation successful"));
		RootPanel.get("oltest2").getElement().getFirstChild().removeFromParent();
		RootPanel.get("oltest2").add(ol);
		RootPanel.get("oltest2").getElement().setAttribute("class", "pass");
	}

	private void runEventTests() {
		runTest6();
		runTest7();
		runTest8();
		runTest9();
	}

	private void runTest9() {
		Object othermessage = new String("Object message");
		EventBus.handleObjectEvent(new ObjectChannelEvent("widget",othermessage));
	}

	private void runTest8() {
		CustomMessage message = new CustomMessage();
		message.setId("test8");
		message.setText("CustomMessage event successful");
		EventBus.EVENT_BUS.fireEvent(new CustomMessageEvent("widget",message));
	}

	private void runTest7() {
		Label[] messages = new Label[3];
		messages[0] = new InlineLabel("Widgets have ");
		messages[1] = new InlineLabel("been ");
		messages[2] = new InlineLabel("added.");
		EventBus.EVENT_BUS.fireEvent(new ComposeEvent("widget","test7",messages));
	}

	private void runTest6() {
		Label success = new Label();
		success.setStyleName("pass");
		success.setText("Widget has been added");
		EventBus.handleEvent("widget", "test6", success);
	}

	private void setupTest6() {
		TestController test6 = new TestController();
		test6.setChannels("widget");
	}
	
	private void setupTest5() {
		TestController controller = new TestController();
		controller.setChannels("event");
	}

	private native void runJsTests() /*-{
		$wnd.RunTests();
	}-*/;
}
