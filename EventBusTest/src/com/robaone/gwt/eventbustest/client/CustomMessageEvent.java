package com.robaone.gwt.eventbustest.client;

import com.google.web.bindery.event.shared.Event;
import com.robaone.gwt.eventbus.client.EventBus;
import com.robaone.gwt.eventbus.client.IgnoreEventException;
import com.robaone.gwt.eventbustest.shared.CustomMessage;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class CustomMessageEvent extends Event<CustomMessageEvent.CustomMessageHandler> {
	public static Type<CustomMessageHandler> TYPE = new Type<CustomMessageEvent.CustomMessageHandler>();
	private String channel;
	private CustomMessage message;
	private int eventid = EventBus.nextEventid();
	public interface CustomMessageHandler {
		public void handleCustomMessage(String channel, CustomMessage message);
		public void setEventid(int eventid) throws IgnoreEventException;
	}
	public CustomMessageEvent(String channel, CustomMessage message) {
		this.setChannel(channel);
		this.setMessage(message);
	}
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public CustomMessage getMessage() {
		return message;
	}

	public void setMessage(CustomMessage message) {
		this.message = message;
	}

	public int getEventid() {
		return eventid;
	}

	public void setEventid(int eventid) {
		this.eventid = eventid;
	}
	@Override
	public com.google.web.bindery.event.shared.Event.Type<CustomMessageHandler> getAssociatedType() {
		return TYPE;
	}
	@Override
	protected void dispatch(CustomMessageHandler handler) {
		if(this.getChannel() != null && this.getChannel().trim().length() > 0){
			try {
				handler.setEventid(this.getEventid());
				handler.handleCustomMessage(getChannel(),getMessage());
			} catch (IgnoreEventException e) {
			}
		}
	}

}
