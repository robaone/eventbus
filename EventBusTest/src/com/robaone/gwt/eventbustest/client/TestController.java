package com.robaone.gwt.eventbustest.client;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.Event.Type;
import com.robaone.gwt.eventbus.client.EventBusConstants;
import com.robaone.gwt.eventbus.client.EventDrivenController;
import com.robaone.gwt.eventbus.client.IgnoreEventException;
import com.robaone.gwt.eventbus.client.ObjectMessageHandler;
import com.robaone.gwt.eventbustest.client.CustomMessageEvent.CustomMessageHandler;
import com.robaone.gwt.eventbustest.shared.CustomMessage;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class TestController extends EventDrivenController{
	public TestController(){
		bind();
		this.m_event_bus.addHandler(CustomMessageEvent.TYPE, new TestEventHandler(this));
	}

	@Override
	public void handleEvent(String command, Widget message) {
		try{
			RootPanel.get(command).getElement().removeChild(RootPanel.get(command).getElement().getFirstChild());
			RootPanel.get(command).add(message);
			RootPanel.get(command).getElement().setAttribute("class", "pass");
		}catch(Exception e){}
	}

	@Override
	public void handleEvent(String command, Widget[] messages) {
		try{
			RootPanel.get(command).getElement().removeChild(RootPanel.get(command).getElement().getFirstChild());
			FlowPanel flow = new FlowPanel();
			for(Widget message : messages){
				flow.add(message);
			}
			RootPanel.get(command).add(flow);
			if(messages.length == 3){
				RootPanel.get(command).getElement().setAttribute("class", "pass");
			}
		}catch(Exception e){}
	}

	@Override
	public void handleEvent(JavaScriptObject message) {
		this.getTest(message);

	}

	private native String getTest(JavaScriptObject message) /*-{
		try{
			var test = message.test;
			$wnd.test5(test);
		}catch(Exception){return null;}
	}-*/;

	

	@Override
	public void handleObjectEvent(Object message) {
		try{
			RootPanel.get("test9").getElement().removeChild(RootPanel.get("test9").getElement().getFirstChild());
			InlineLabel label = new InlineLabel(message.toString());
			RootPanel.get("test9").add(label);
			RootPanel.get("test9").getElement().setAttribute("class", "pass");
		}catch(Exception e){}
	}

	public void handleCustomMessage(CustomMessage message) {
		try{
			RootPanel.get(message.getId()).getElement().removeChild(RootPanel.get(message.getId()).getElement().getFirstChild());
			InlineLabel label = new InlineLabel(message.getText());
			RootPanel.get(message.getId()).add(label);
			RootPanel.get(message.getId()).getElement().setAttribute("class", "pass");
		}catch(Exception e){}
	}

}
