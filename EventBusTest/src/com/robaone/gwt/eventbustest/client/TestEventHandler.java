package com.robaone.gwt.eventbustest.client;

import com.robaone.gwt.eventbus.client.IgnoreEventException;
import com.robaone.gwt.eventbustest.client.CustomMessageEvent.CustomMessageHandler;
import com.robaone.gwt.eventbustest.shared.CustomMessage;

public final class TestEventHandler implements CustomMessageHandler {
	private TestController controller;
	public interface Handler {
		public void handleCustomMessage(CustomMessage message);
		public boolean inChannel(String channel);
		public void setEventid(int eventid);
	}
	public TestEventHandler(TestController testController){
		this.controller = testController;
	}
	@Override
	public void handleCustomMessage(String channel, CustomMessage message) {
		try{
			if(controller.inChannel(channel)){
				controller.handleCustomMessage(message);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void setEventid(int eventid) throws IgnoreEventException {
		controller.setEventid(eventid);
	}
}