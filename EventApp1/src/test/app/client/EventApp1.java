package test.app.client;

import test.app.client.widget.EditableLabelUi;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EventApp1 implements EntryPoint {
	

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		EditableLabelUi label = new EditableLabelUi();
		/**
		 * Listen to messages from 'item1'
		 */
		label.setChannels("item1");
		/**
		 * Broadcast messages to 'item1'
		 */
		label.setBroadcastChannel("item1");
		RootPanel.get().add(label);
	}
}
