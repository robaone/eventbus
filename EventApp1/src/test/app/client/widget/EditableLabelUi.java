package test.app.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.eventbus.client.EventBus;
import com.robaone.gwt.eventbus.client.EventDrivenComposite;

public class EditableLabelUi extends EventDrivenComposite implements HasText {

	private static EditableLabelUiUiBinder uiBinder = GWT
			.create(EditableLabelUiUiBinder.class);

	interface EditableLabelUiUiBinder extends UiBinder<Widget, EditableLabelUi> {
	}

	private String m_broadcastchannel;

	public EditableLabelUi() {
		initWidget(uiBinder.createAndBindUi(this));
		bind();
	}
	
	@Override
	public void bind(){
		super.bind();
		textbox.addChangeHandler(new ChangeHandler(){

			@Override
			public void onChange(ChangeEvent event) {
				//Use event bus
				JSONObject msg = new JSONObject();
				msg.put("text", new JSONString(textbox.getText()));
				EventBus.handleEvent(getBroadcastChannel(), msg.getJavaScriptObject());
			}
			
		});
		textbox.addBlurHandler(new BlurHandler(){

			@Override
			public void onBlur(BlurEvent event) {
				view();
			}
			
		});
		edit.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				edit();
			}
			
		});
		view();
	}

	protected String getBroadcastChannel() {
		return m_broadcastchannel;
	}
	
	public void setBroadcastChannel(String channel){
		this.m_broadcastchannel = channel;
	}

	protected void edit() {
		panel.remove(edit);
		panel.remove(label);
		panel.add(textbox);
	}

	@UiField Label label;
	@UiField FlowPanel panel;
	Button edit = new Button("edit");
	TextBox textbox = new TextBox();

	public EditableLabelUi(String string) {
		initWidget(uiBinder.createAndBindUi(this));
		setText(string);
		bind();
	}

	@UiHandler("label")
	void onClick(ClickEvent e) {
		edit();
	}
	
	public void setText(String text) {
		label.setText(text);
		textbox.setText(text);
		if(text.length() > 0){
			panel.remove(edit);
		}else if(label.isAttached()){
			panel.add(edit);
		}
	}

	public String getText() {
		return label.getText();
	}

	@Override
	public void handleEvent(String command, Composite message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleEvent(JavaScriptObject message) {
		JSONObject msg = new JSONObject(message);
		try{
			String text = msg.get("text").isString().stringValue();
			setText(text);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void handleEvent(String command, Composite[] messages) {
		// TODO Auto-generated method stub
		
	}

	private void view() {
		panel.remove(textbox);
		panel.add(label);
		if(getText().trim().length() == 0){
			panel.add(edit);
		}else{
			panel.remove(edit);
		}
	}

}
