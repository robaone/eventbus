package com.robaone.gwt.eventbus.client;

import com.google.web.bindery.event.shared.Event;

public class ObjectChannelEvent extends Event<ObjectMessageHandler> {
	private String channel;
	private Object message;
	private int eventid = EventBus.nextEventid();
	public static final Type<ObjectMessageHandler> TYPE = new Type<ObjectMessageHandler>();
	
	public ObjectChannelEvent(String channel, Object message) {
		this.setChannel(channel);
		this.setMessage(message);
	}

	public com.google.web.bindery.event.shared.Event.Type<ObjectMessageHandler> getAssociatedType(){
		return TYPE;
	}
	
	@Override
	protected void dispatch(ObjectMessageHandler handler) {
		if(this.getChannel() != null && this.getChannel().trim().length() > 0){
			try {
				handler.setEventid(this.getEventid());
				handler.handleNative(getChannel(),getMessage());
			} catch (IgnoreEventException e) {
			}
		}
	}
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Object getMessage() {
		return message;
	}

	public void setMessage(Object message) {
		this.message = message;
	}

	public int getEventid() {
		return eventid;
	}

	public void setEventid(int eventid) {
		this.eventid = eventid;
	}
}
