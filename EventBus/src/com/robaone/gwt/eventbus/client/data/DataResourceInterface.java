package com.robaone.gwt.eventbus.client.data;

import com.google.gwt.core.client.JavaScriptObject;

public interface DataResourceInterface<T extends JavaScriptObject> {

	void doGet(DataResourceHandler<T> dataResourceHandler) throws Exception;
	void doPost(DataResourceHandler<T> dataResourceHandler) throws Exception;
	void doDelete(DataResourceHandler<T> dataResourceHandler) throws Exception;
	void doUpdate(DataResourceHandler<T> dataResourceHandler) throws Exception;
}
