package com.robaone.gwt.eventbus.client.data;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class GWTDSResponse<T extends JavaScriptObject> extends JavaScriptObject {
	protected GWTDSResponse(){
		
	}
	public final native int getStatus()/*-{
		return this.response.status;
	}-*/;
	
	public final native JsArray<T> getData()/*-{
		return this.response.data;
	}-*/;
	
	public final native int getTotalRows()/*-{
		return this.response.totalRows;
	}-*/;
	
	public final native int getEndRow()/*-{
		return this.response.endRow;
	}-*/;
	
	public final native String getError()/*-{
		return this.response.error;
	}-*/;
	
	public final native JavaScriptObject getErrors()/*-{
		return this.response.errors;
	}-*/;
	
	public final native int getPage()/*-{
		return this.response.page;
	}-*/;
	
	public final native int getStartRow()/*-{
		return this.response.startRow;
	}-*/;
	
	public static final native GWTDSResponse<?> buildResponse(String json)/*-{
		return eval('(' + json + ')');
	}-*/;
}
