package com.robaone.gwt.eventbus.client.data;

import com.google.gwt.core.client.JavaScriptObject;

public interface DataResourceHandler<T extends JavaScriptObject> {
	public void handleSuccess(GWTDSResponse<T> response);
	public void handleFailure(Throwable e);
}
