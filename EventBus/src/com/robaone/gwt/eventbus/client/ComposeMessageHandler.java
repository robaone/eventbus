package com.robaone.gwt.eventbus.client;

import com.google.gwt.user.client.ui.Widget;

public interface ComposeMessageHandler {
	public void handle(String channel, String command, Widget widget);

	public void handle(String channel, String command, Widget[] message);

	public void setEventid(int eventid) throws IgnoreEventException;
}
