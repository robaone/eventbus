package com.robaone.gwt.eventbus.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EventBus implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public static com.google.gwt.event.shared.EventBus EVENT_BUS = GWT.create(SimpleEventBus.class);
	static int item_index = 0;
	private static int eventid = 0;
	public void onModuleLoad() {
		createEventMethod();
	}
	
	public synchronized static int nextEventid(){
		eventid++;
		return eventid;
	}
	
	public native void createEventMethod()/*-{
		if(!$wnd.Rolib){
			$wnd.Rolib = {"eventbus":{"handleEvent":
				function(channel,message){
            		@com.robaone.gwt.eventbus.client.EventBus::handleEvent(Ljava/lang/String;Lcom/google/gwt/core/client/JavaScriptObject;)(channel,message);
    			}
			}
		  };
		}
	}-*/;
	
	public static void handleEvent(String channel,String command,Widget widget){
		System.out.println("channel = "+channel);
		EVENT_BUS.fireEvent(new ComposeEvent(channel,command,widget));
	}
	public static void handleEvent(String channel,JavaScriptObject message){
		System.out.println("channel = "+channel);
		EVENT_BUS.fireEvent(new ChannelEvent(channel,message));
	}
	public static void handleObjectEvent(@SuppressWarnings("rawtypes") ObjectChannelEvent event){
		System.out.println("channel = "+event.getChannel());
		EVENT_BUS.fireEvent(event);
	}
}
