package com.robaone.gwt.eventbus.client;

public interface ObjectMessageHandler {

	void handleNative(String channel, Object message);

	void setEventid(int eventid) throws IgnoreEventException;

}
