package com.robaone.gwt.eventbus.client;

import java.util.HashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.ResettableEventBus;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public abstract class EventDrivenComposite extends Composite {
	private HashMap<String,Boolean> m_channels = new HashMap<String,Boolean>();
	private ChannelMessageHandler m_handler1;
	private ComposeMessageHandler m_handler2;
	private ResettableEventBus m_event_bus;
	private int eventid = 0;

	protected void bind() {
		if(this.m_event_bus == null){
			this.m_event_bus = new ResettableEventBus(EventBus.EVENT_BUS);
		}
		this.m_event_bus.addHandler(ChannelEvent.TYPE, getHandler1());
		this.m_event_bus.addHandler(ComposeEvent.TYPE, getHandler2());
		this.m_event_bus.addHandler(ObjectChannelEvent.TYPE, getHandler3());
	}
	
	private ObjectMessageHandler getHandler3() {
		return new ObjectMessageHandler(){
			@Override
			public void handleNative(String channel, Object message) {
				try{
					if(inChannel(channel)){
						handleObjectEvent(message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void setEventid(int eventid) throws IgnoreEventException {
				EventDrivenComposite.this.setEventid(eventid);
			}
		};
	}
	protected void unbind() {
		this.m_event_bus.removeHandlers();
	}
	private ComposeMessageHandler getHandler2() {
		this.m_handler2 =  new ComposeMessageHandler(){

			@Override
			public void handle(String channel, String command,
					Widget message) {
				try{
					if(inChannel(channel)){
						handleEvent(command,message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void handle(String channel, String command,
					Widget[] messages) {
				try{
					if(inChannel(channel)){
						handleEvent(command,messages);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void setEventid(int eventid) throws IgnoreEventException {
				EventDrivenComposite.this.setEventid(eventid);
			}			
		};
		return this.m_handler2;
	}
	private ChannelMessageHandler getHandler1() {
		this.m_handler1 = new ChannelMessageHandler(){

			@Override
			public void handle(String channel, JavaScriptObject message) {
				try{
					if(inChannel(channel)){
						handleEvent(message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void handle(String channel, String command,
					Composite message) {
				try{
					if(inChannel(channel)){
						handleEvent(command,message);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void handle(String channel, String command,
					Composite[] messages) {
				try{
					if(inChannel(channel)){
						handleEvent(command,messages);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void setEventid(int eventid) throws IgnoreEventException {
				EventDrivenComposite.this.setEventid(eventid);
			}
			
		};
		return this.m_handler1;
	}
	abstract public void handleEvent(String command, Widget message);
	abstract public void handleEvent(JavaScriptObject message);
	abstract public void handleEvent(String command, Widget[] messages);
	abstract public void handleObjectEvent(Object message);
	public void setChannels(String channels){
		String[] split = channels.split(" ");
		for(String str : split){
			this.m_channels.put(str, new Boolean(true));
		}
	}

	protected boolean inChannel(String channel) {
		try{
			return this.m_channels.get(channel);
		}catch(Exception e){
			return false;
		}
	}
	public int getEventid() {
		return eventid;
	}
	public void setEventid(int eventid) throws IgnoreEventException {
		System.out.println("Current eventid = "+this.eventid+": Incomming eventid = "+eventid);
		if(this.eventid >= eventid){
			throw new IgnoreEventException("Event already triggered");
		}
		this.eventid = eventid;
	}
}
