package com.robaone.gwt.eventbus.client;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.web.bindery.event.shared.Event;

public class ChannelEvent extends Event<ChannelMessageHandler> {
	private String channel;
	private JavaScriptObject message;
	public static Type<ChannelMessageHandler> TYPE = new Type<ChannelMessageHandler>();
	private int eventid = EventBus.nextEventid();
	
	public ChannelEvent(String channel, JavaScriptObject message) {
		this.setChannel(channel);
		this.setMessage(message);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<ChannelMessageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ChannelMessageHandler handler) {
		if(this.getChannel() != null && this.getChannel().trim().length() > 0){
			try {
				handler.setEventid(this.getEventid());
				handler.handle(getChannel(),getMessage());
			} catch (IgnoreEventException e) {
				
			}
		}
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public JavaScriptObject getMessage() {
		return message;
	}

	public void setMessage(JavaScriptObject message) {
		this.message = message;
	}

	public int getEventid() {
		return eventid;
	}

	public void setEventid(int eventid) {
		this.eventid = eventid;
	}

}
