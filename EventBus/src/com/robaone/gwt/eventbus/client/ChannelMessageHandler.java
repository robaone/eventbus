package com.robaone.gwt.eventbus.client;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.ui.Composite;

public interface ChannelMessageHandler {

	public void handle(String channel, JavaScriptObject message);

	public void handle(String channel, String command, Composite message);
	
	public void handle(String channel, String command, Composite[] messages);

	public void setEventid(int eventid) throws IgnoreEventException;

}
