package com.robaone.gwt.eventbus.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.Event;

public class ComposeEvent extends Event<ComposeMessageHandler> {
	private String channel;
	private Widget[] message;
	private String m_command = APPEND;
	public static final String APPEND = "APPEND";
	public static final String REPLACE = "REPLACE";
	public static final String CLEAR = "CLEAR";
	public static final String HIDE = "HIDE";
	public static final String SHOW = "SHOW";
	public static final String REMOVE = "REMOVE";
	private int eventid = EventBus.nextEventid();
	
	public static Type<ComposeMessageHandler> TYPE = new Type<ComposeMessageHandler>();
	
	public ComposeEvent(String channel, Composite message) {
		this.setChannel(channel);
		this.setMessage(message);
	}
	
	public ComposeEvent(String channel, Widget[] messages) {
		this.setChannel(channel);
		this.setMessages(messages);
	}
	
	private void setMessages(Widget[] messages) {
		this.message = messages;
	}

	public ComposeEvent(String channel,String command, Widget widget){
		this.setChannel(channel);
		this.setMessage(widget);
		this.setCommand(command);
	}
	
	public ComposeEvent(String channel,String command, Widget[] messages){
		this.setChannel(channel);
		this.setMessages(messages);
		this.setCommand(command);
	}

	private void setCommand(String command) {
		this.m_command = command;
	}
	
	private String getCommand(){
		return this.m_command;
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<ComposeMessageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ComposeMessageHandler handler) {
		if(this.getChannel() != null && this.getChannel().trim().length() > 0){
			try {
				handler.setEventid(this.getEventid());
				if(this.message.length > 1){
					handler.handle(getChannel(), getCommand(), this.message);
				}else{
					handler.handle(getChannel(),getCommand(),getMessage());
				}
			} catch (IgnoreEventException e) {
			}
		}
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Widget getMessage() {
		if(message != null && message.length > 0){
			return message[0];
		}else{
			return null;
		}
	}

	public void setMessage(Widget widget) {
		this.message = new Widget[1];
		this.message[0] = widget;
	}

	public int getEventid() {
		return eventid;
	}

	public void setEventid(int eventid) {
		this.eventid = eventid;
	}

}
