package com.robaone.gwt.eventbus.client.model;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.robaone.gwt.eventbus.client.controller.PagedListController;

public class PagedData {
	protected JavaScriptObject rsjo;
	private String m_source;
	private String m_error;
	public PagedData(){
	}
	public JsArray<JavaScriptObject> getData() {
		try{
			return this.getJSData(rsjo);
		}catch(Exception e){
			return null;
		}
	}
	public final native JsArray<JavaScriptObject> getJSData(JavaScriptObject rsjo)/*-{
		return response.daa;
	}-*/;
	public static final native JavaScriptObject eval(String response)/*-{
		return eval('('+response+')');
	}-*/;
	public int getTotalrows() {
		try{
			return getTotalrows(rsjo);
		}catch(Exception e){
			return 0;
		}
	}
	public final native int getTotalrows(JavaScriptObject jo)/*-{
		return jo.response.totalRows;
	}-*/;
	public int getEndrow() {
		try{
			return getEndrow(rsjo);
		}catch(Exception e){
			return 0;
		}
	}
	private final native int getEndrow(JavaScriptObject jo)/*-{
		return jo.response.endRow;
	}-*/;
	public int getStartrow() {
		try{
			return getStartrow(rsjo);
		}catch(Exception e){
			return 0;
		}
	}
	private final native int getStartrow(JavaScriptObject jo)/*-{
		return jo.response.startRow;
	}-*/;
	public void setSrc(String string) {
		this.m_source = string;
	}
	public void onLoad(PagedListController pagedListController) {
		pagedListController.load(this);
	}
	
	public void load(final PagedListController controller){
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET,this.m_source);
		try {
			rb.sendRequest("limit="+controller.getLimit()+"&page="+controller.getPage(), new RequestCallback(){

				@Override
				public void onResponseReceived(Request request, Response response) {
					try{
						if(response.getStatusCode() == 200){
					
						rsjo = eval(response.getText());
						controller.load(PagedData.this);
					}else{
						onError(request,new Exception(response.getStatusText()));
					}
					}catch(Exception e){
						onError(request,e);
					}
				}

				@Override
				public void onError(Request request, Throwable exception) {
					setError(exception.getMessage());
					controller.load(PagedData.this);
				}
				
			});
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}
	protected void setError(String message) {
		this.m_error = message;
	}
	public String getError(){
		return this.m_error;
	}
}
