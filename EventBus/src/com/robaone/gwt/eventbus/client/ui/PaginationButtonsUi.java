package com.robaone.gwt.eventbus.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.eventbus.client.widget.UnorderedListWidget;

public class PaginationButtonsUi extends Composite {

	private static PaginationButtonsUiUiBinder uiBinder = GWT
			.create(PaginationButtonsUiUiBinder.class);

	interface PaginationButtonsUiUiBinder extends
			UiBinder<Widget, PaginationButtonsUi> {
	}

	public PaginationButtonsUi() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	UnorderedListWidget list;
	
	public UnorderedListWidget getList(){
		return list;
	}
}
