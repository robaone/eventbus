package com.robaone.gwt.eventbus.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.dom.client.TableRowElement;
import com.google.gwt.dom.client.TableSectionElement;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class DynamicTableWidget extends ComplexPanel {
	TableSectionElement thead;
	TableSectionElement tbody;
	public DynamicTableWidget(){
		setElement(Document.get().createTableElement());
		thead = Document.get().createTHeadElement();
		tbody = Document.get().createTBodyElement();
		getElement().appendChild(thead);
		getElement().appendChild(tbody);
	}
	
	@Override
	public void clear(){
		this.setRows(new DynamicTableRowWidget[0]);
	}
	public void setId(String id){
		getElement().setId(id);
	}
	
	public void setStyleName(String className){
		getElement().setClassName(className);
	}
	
	public void setHeaders(String[] headers){
		TableRowElement tr = Document.get().createTRElement();
		for(String header : headers){
			TableCellElement th = Document.get().createTHElement();
			th.setInnerText(header);
			tr.appendChild(th);
		}
		thead.appendChild(tr);
	}
	
	public void setHeaders(Widget[] headers){
		TableRowElement tr = Document.get().createTRElement();
		for(Widget header : headers){
			TableCellElement th = Document.get().createTHElement();
			tr.appendChild(th);
			th.appendChild(header.getElement());
		}
		thead.appendChild(tr);
	}
	
	public void appendRow(DynamicTableRowWidget tr){
		tbody.appendChild(tr.getElement());
	}
	
	public void setRows(DynamicTableRowWidget[] rows){
		int existingRows = tbody.getChildCount();
		for(int i = 0; i < rows.length;i++){
			if(i < existingRows){
				tbody.replaceChild(rows[i].getElement(), tbody.getChild(i));
			}else{
				this.appendRow(rows[i]);
			}
		}
		for(int i = rows.length;i < existingRows;i++){
			Node extraRow = tbody.getLastChild();
			tbody.removeChild(extraRow);
		}
	}
}
