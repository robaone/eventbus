package com.robaone.gwt.eventbus.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.TableCellElement;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * <pre>   Copyright 2013 Ansel Robateau
         http://www.robaone.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.</pre>
 * @author Ansel
 *
 */
public class DynamicTableRowWidget extends Composite {
	private class TableRow extends ComplexPanel {
		public TableRow(){
			setElement(Document.get().createTRElement());
		}
		public void setRow(Widget[] widgets){
			for(Widget widget : widgets){
				TableCellElement td = Document.get().createTDElement();
				this.getElement().appendChild(td);
				td.appendChild(widget.getElement());
			}
		}
		public void setRow(String[] celldata) {
			for(String str : celldata){
				TableCellElement td = Document.get().createTDElement();
				this.getElement().appendChild(td);
				td.setInnerText(str);
			}
		}
	}
	TableRow row;
	public DynamicTableRowWidget(){
		row = new TableRow();
		this.initWidget(row);
		
	}
	public void setContent(Widget[] celldata){
		row.setRow(celldata);
	}
	public void setContent(String[] celldata){
		row.setRow(celldata);
	}
	public void removeFromTable(){
		row.getElement().removeFromParent();
	}
}
