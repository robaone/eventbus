package com.robaone.gwt.eventbus.client.controller;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Widget;
import com.robaone.gwt.eventbus.client.EventDrivenController;
import com.robaone.gwt.eventbus.client.model.PagedData;
import com.robaone.gwt.eventbus.client.ui.PaginationButtonsUi;
import com.robaone.gwt.eventbus.client.widget.ListItemWidget;

public class PagedListController extends EventDrivenController {
	private PaginationButtonsUi m_ui;
	private PagedData m_model;
	private int m_limit = 5;
	private int m_page = 1;
	ListItemWidget prev,next;
	
	public PagedListController(PaginationButtonsUi ui,PagedData data){
		super();
		this.m_ui = ui;
		this.m_model = data;
		prev = new ListItemWidget();
		next = new ListItemWidget();
		prev.add(new Anchor("Prev"));
		next.add(new Anchor("Next"));
	}
	
	public void setPage(int page){
		this.m_page = page;
	}
	
	public void setLimit(int limit){
		this.m_limit = limit;
	}
	
	public void load(PagedData data){
		this.m_model = data;
		/**
		 * Setup the array of buttons
		 */
		this.m_ui.getList().clear();
		if(this.getPage() == 1){
			prev.addStyleName("disabled");
		}
		int totalrows = m_model.getTotalrows();
		int pages = totalrows / getLimit();
		if(getPage() >= pages){
			next.addStyleName("disabled");
		}
		this.m_ui.getList().add(prev);
		/**
		 * Add list of pages
		 */
		//int page_buttons = (pages > 7) ? 7 : pages;
		int separator1 = getPage() > 4 ? getPage()-2 : 0;
		separator1 = separator1 - (4 - (pages - getPage() > 3 ? 3 : pages - getPage()));
		int separator2 = totalrows - (getLimit() * getPage()) > 2 ? (getPage() + 3) : 0;
		for(int i = 0; i < pages;i++){
			if(separator1 > 0 && (i+1 == separator1 || i+1 < getPage()-(4 - (pages - getPage() > 3 ? 3 : pages - getPage()))) && i > 0){
				if(separator1 == i+1){
					ListItemWidget page = new ListItemWidget();
					page.add(new Anchor(".."));
					page.addStyleName("disabled");
					this.m_ui.getList().add(page);
				}
				continue;
			}
			if(separator2 > 0 && separator2 <= i+1 && (i+1 < pages)){
				if(separator2 == i+1){
					ListItemWidget page = new ListItemWidget();
					page.add(new Anchor(".."));
					page.addStyleName("disabled");
					this.m_ui.getList().add(page);
				}
				continue;
			}
			ListItemWidget page = new ListItemWidget();
			page.add(new Anchor(""+(i+1)));
			if(i+1 == this.getPage()){
				page.addStyleName("disabled");
			}
			this.m_ui.getList().add(page);
		}
		
		this.m_ui.getList().add(next);
	}

	public int getLimit() {
		return m_limit;
	}

	public int getPage() {
		return m_page;
	}

	@Override
	public void handleEvent(String command, Widget message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleEvent(JavaScriptObject message) {
		PagedListMessage_JSO msg = (PagedListMessage_JSO)message;
		
		this.load(msg.getdata());
	}

	@Override
	public void handleEvent(String command, Widget[] messages) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleObjectEvent(Object message) {
		// TODO Auto-generated method stub
		
	}

}
