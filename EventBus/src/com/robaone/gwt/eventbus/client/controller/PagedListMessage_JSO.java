package com.robaone.gwt.eventbus.client.controller;

import com.google.gwt.core.client.JavaScriptObject;
import com.robaone.gwt.eventbus.client.model.PagedData;

public final class PagedListMessage_JSO extends JavaScriptObject {
    protected PagedListMessage_JSO(){
    }
    public final native String getaction()/*-{
        return this.action;
    }-*/;
    public native void  setaction(String value)/*-{
        this.action = value;
    }-*/;
    public final native PagedData getdata()/*-{
        return this.data;
    }-*/;
    public native void  setdata(PagedData value)/*-{
        this.data = value;
    }-*/;
}