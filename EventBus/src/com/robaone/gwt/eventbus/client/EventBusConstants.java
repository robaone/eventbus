package com.robaone.gwt.eventbus.client;

import com.google.web.bindery.event.shared.Event.Type;

public interface EventBusConstants {
	public static final Type<ObjectMessageHandler> OBJECT_TYPE = new Type<ObjectMessageHandler>();
}
