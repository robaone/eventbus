package com.robaone.gwt.eventbus.client;

public class IgnoreEventException extends Exception {

	private static final long serialVersionUID = 3475366660524368150L;

	public IgnoreEventException(String string) {
		super(string);
	}

}
